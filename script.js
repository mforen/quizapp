const questions = [
    {
        question: "Question 1: What is 2 + 2?",
        options: ["3", "4", "5", "6"],
        correctAnswer: 1
    },
    {
        question: "Question 2: What is the capital of France?",
        options: ["Berlin", "Madrid", "Paris", "Rome"],
        correctAnswer: 2
    },
    {
        question: "Question 3: What is 10 x 5?",
        options: ["25", "45", "50", "55"],
        correctAnswer: 2
    },
];

let currentQuestion = 0;
let score = 0;

function loadQuestion() {
    const questionText = document.getElementById('question-text');
    const optionsContainer = document.querySelector('.options-container');
    const options = questions[currentQuestion].options;

    questionText.innerText = questions[currentQuestion].question;
    optionsContainer.innerHTML = '';

    for (let i = 0; i < options.length; i++) {
        const optionButton = document.createElement('button');
        optionButton.classList.add('option');
        optionButton.innerText = options[i];
        optionButton.setAttribute('data-index', i);
        optionButton.addEventListener('click', function () {
            checkAnswer(this);
        });
        optionsContainer.appendChild(optionButton);
    }
}

function checkAnswer(selectedOption) {
    const selectedAnswerIndex = selectedOption.getAttribute('data-index');
    const correctAnswerIndex = questions[currentQuestion].correctAnswer;

    if (selectedAnswerIndex == correctAnswerIndex) {
        score++;
    }

    showResult(selectedAnswerIndex == correctAnswerIndex);
    currentQuestion++;
}

function showResult(isCorrect) {
    const resultText = document.getElementById('result-text');
    const resultContainer = document.querySelector('.result-container');
    const nextButton = document.getElementById('next-button');

    resultText.innerText = isCorrect ? "Correct!" : "Wrong!";
    resultContainer.style.display = 'block';
    nextButton.style.display = 'block';
}

function nextQuestion() {
    if (currentQuestion < questions.length) {
        loadQuestion();
        const resultContainer = document.querySelector('.result-container');
        const nextButton = document.getElementById('next-button');

        resultContainer.style.display = 'none';
        nextButton.style.display = 'none';
    } else {
        endQuiz();
    }
}

function endQuiz() {
    const quizContainer = document.querySelector('.quiz-container');
    quizContainer.innerHTML = `<h2>Quiz Completed</h2><p>Your Score: ${score} out of ${questions.length}</p>`;
}

loadQuestion();
